/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package products;

/**
 *
 * @author Dorme Ornimus
 */
public interface SellAble {
    public Double getPrice();
    public String getName();
}
