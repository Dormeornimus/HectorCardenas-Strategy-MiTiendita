/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package products;

/**
 *
 * @author Dorme Ornimus
 */
public abstract class Product implements SellAble {
    protected Double price;
    protected String name;
    
    @Override
    public Double getPrice() {
        return price;
    }
    
    @Override
    public String getName(){
        return name;
    }
    
    public String toString(){
        return this.name +" "+ this.price +"$" + " Each";
    }
}
