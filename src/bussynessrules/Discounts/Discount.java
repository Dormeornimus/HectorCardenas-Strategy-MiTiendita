/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bussynessrules.Discounts;

import bussynessrules.behaviours.DiscountBehaviour;

/**
 *
 * @author Dorme Ornimus
 */
public abstract class Discount implements DiscountBehaviour{
    protected Double discount = 0.0;
    protected Double percent = 0.0;
    protected String name = "";
    
    public String toString(){
        return this.name+ " = "+ this.percent*100 + "%" ;
    }
}
