/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bussynessrules.Discounts;

import java.util.List;
import javafx.util.Pair;
import products.Fruit;
import products.SellAble;

/**
 *
 * @author Dorme Ornimus
 */
public class FruitDiscount extends Discount{
    
    public FruitDiscount(Double percent){
        this.name = "Fruit Discount";
        this.percent = percent/100;
    }
    
    @Override
    public double getDiscount(List<Pair<Double, SellAble>> saleList) {
        saleList.stream().filter((o) -> (o.getValue() instanceof Fruit)).forEachOrdered((o) -> {
            this.discount += o.getValue().getPrice()*o.getKey()*this.percent;
        });
        return discount;
    }
    
}
