/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bussynessrules.Discounts;

import java.util.List;
import javafx.util.Pair;
import products.SellAble;

/**
 *
 * @author Dorme Ornimus
 */
public class ElderlyDiscount extends Discount{
    
    public ElderlyDiscount (Double percent){
        this.name = "Elderly Discount";
        this.percent = percent/100;
    }

    @Override
    public double getDiscount(List<Pair<Double, SellAble>> saleList) {
        saleList.forEach((o) -> {
            this.discount += o.getValue().getPrice()*o.getKey()*percent;
        });
        return discount;
    }
    
}
