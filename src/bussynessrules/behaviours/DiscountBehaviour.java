package bussynessrules.behaviours;

import java.util.List;
import javafx.util.Pair;
import products.SellAble;

/**
 *
 * @author Dorme Ornimus
 */
public interface DiscountBehaviour {
    public double getDiscount(List<Pair<Double,SellAble>> saleList);
}
