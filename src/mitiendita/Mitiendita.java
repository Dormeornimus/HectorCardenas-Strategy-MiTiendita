/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mitiendita;

import domain.Sale;
import products.Apple;
import products.Banana;
import products.Butter;
import products.Chesse;
import products.Cream;
import products.Milk;

/**
 *
 * @author Dorme Ornimus
 */
public class Mitiendita {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Venta Lunes
        Sale lunes = new Sale(1);
        lunes.addItem(10.0, new Milk());
        lunes.addItem(0.5, new Cream());
        lunes.addItem(0.250, new Butter());
        lunes.addItem(0.50, new Chesse());
        lunes.addItem(1.0, new Banana());
        lunes.addItem(2.0, new Apple());
        System.out.println("Sale On Monday \n");
        System.out.println(lunes.toString());
        System.out.println("");
        
        
        //Venta Martes
        Sale martes = new Sale(2);
        martes.addItem(10.0, new Milk());
        martes.addItem(0.5, new Cream());
        martes.addItem(0.250, new Butter());
        martes.addItem(0.50, new Chesse());
        martes.addItem(1.0, new Banana());
        martes.addItem(2.0, new Apple());
        System.out.println("Sale On Tuesday \n");
        System.out.println(martes.toString());
        System.out.println("");
        
        //Venta Miercoles
        Sale miercoles = new Sale(3);
        miercoles.addItem(10.0, new Milk());
        miercoles.addItem(0.5, new Cream());
        miercoles.addItem(0.250, new Butter());
        miercoles.addItem(0.50, new Chesse());
        miercoles.addItem(1.0, new Banana());
        miercoles.addItem(2.0, new Apple());
        System.out.println("Sale On Wednesday \n");
        System.out.println(miercoles.toString());
        System.out.println("");
        
        //Venta Jueves
        Sale jueves = new Sale(4);
        jueves.addItem(10.0, new Milk());
        jueves.addItem(0.5, new Cream());
        jueves.addItem(0.250, new Butter());
        jueves.addItem(0.50, new Chesse());
        jueves.addItem(1.0, new Banana());
        jueves.addItem(2.0, new Apple());
        System.out.println("Sale On Thursday \n");
        System.out.println(jueves.toString());
        System.out.println("");
        
        //Venta Viernes
        Sale viernes = new Sale(5);
        viernes.addItem(10.0, new Milk());
        viernes.addItem(0.5, new Cream());
        viernes.addItem(0.250, new Butter());
        viernes.addItem(0.50, new Chesse());
        viernes.addItem(1.0, new Banana());
        viernes.addItem(2.0, new Apple());
        System.out.println("Sale On Friday \n");
        System.out.println(viernes.toString());
        System.out.println("");
        
        //Venta Sabado
        Sale sabado = new Sale(6);
        sabado.addItem(10.0, new Milk());
        sabado.addItem(0.5, new Cream());
        sabado.addItem(0.250, new Butter());
        sabado.addItem(0.50, new Chesse());
        sabado.addItem(1.0, new Banana());
        sabado.addItem(2.0, new Apple());
        System.out.println("Sale On Saturday \n");
        System.out.println(sabado.toString());
        System.out.println("");
        
        //Venta Domingo
        Sale domingo = new Sale(7);
        domingo.addItem(10.0, new Milk());
        domingo.addItem(0.5, new Cream());
        domingo.addItem(0.250, new Butter());
        domingo.addItem(0.50, new Chesse());
        domingo.addItem(1.0, new Banana());
        domingo.addItem(2.0, new Apple());
        System.out.println("Sale On Sunday \n");
        System.out.println(domingo.toString());
        System.out.println("");
    }
    
}
