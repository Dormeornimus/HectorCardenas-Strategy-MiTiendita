package domain;

import bussynessrules.Discounts.DairyDiscount;
import bussynessrules.Discounts.ElderlyDiscount;
import bussynessrules.Discounts.FruitDiscount;
import bussynessrules.Discounts.InlayDiscount;
import java.util.ArrayList;
import java.util.List;
import javafx.util.Pair;
import products.SellAble;
import bussynessrules.behaviours.DiscountBehaviour;

/**
 *
 * @author Dorme Ornimus
 */
public final class Sale {
    private List<Pair<Double,SellAble>> items;
    private List<DiscountBehaviour> discounts;
    private Double subtotal;
    private Double total;
    private Double totalDiscount;
    private final Double iva = 0.16;
    
    public Sale(){
        this.items = new ArrayList<>();
        this.discounts = new ArrayList<>();
        this.total = 0.0;
        this.subtotal = 0.0;
    }
    
    public Sale(Integer day) {
        this.items = new ArrayList<>();
        this.discounts = new ArrayList<>();
        this.total = 0.0;
        this.subtotal = 0.0;
        switch(day){
            case 1:
                setDiscount(new ElderlyDiscount(5.0));
                break;
            case 2:
                break;
            case 3:
                setDiscount(new DairyDiscount(10.0));
                break;
            case 4:
                setDiscount(new FruitDiscount(15.0));
                break;
            case 5:
                setDiscount(new DairyDiscount(15.0));
                setDiscount(new InlayDiscount(5.0));
                break;
            case 6:
                break;
            case 7:
                setDiscount(new ElderlyDiscount(5.0));
                break;
        }
    }
    
    
    public void addItem(Double quantity, SellAble product){
        Pair a= new Pair<>(quantity,product);
        this.items.add(a);
    }
    
    
    public void calculateTotal(){
        this.items.forEach((o) -> {
            this.subtotal += o.getKey()*o.getValue().getPrice();
        });
        this.total = this.subtotal*(1+this.iva)-calculateTotalDiscount(items);
    }
    
    public Double getTotal(){
        if (this.total == 0.0){
            calculateTotal();
        }
        return total;
    }
    
    public Double calculateTotalDiscount(List<Pair<Double,SellAble>> items){
        this.totalDiscount = 0.0;
        discounts.forEach((o) -> {
            this.totalDiscount +=o.getDiscount(items);
        });
        totalDiscount = totalDiscount*(1+iva);
        return this.totalDiscount;
    }
    
    public void setDiscount(DiscountBehaviour discount){
        if(!discounts.contains(discount)){
            this.discounts.add(discount);
        }
        else{
        System.out.println("This discount is already in the discounts, discounts can't be repeated");
        }
    }
    
    public void removeItem (SellAble item){
        for(Pair<Double,SellAble> o : this.items){
            if (o.getValue().equals(item));
                items.remove(o);
        }
    }
    
    public void removeDiscount(DiscountBehaviour discount){
        if(this.discounts.contains(discount)){
            this.discounts.remove(discount);
        }
    }
    
    @Override
    public String toString(){
        String ticket = "";
        if (this.total == 0.0){
            calculateTotal();
        }
        
        ticket = this.items.stream().map((o) -> o.getKey().toString() + " - " + o.getValue().toString()+"\n").reduce(ticket, String::concat);
        ticket +="\n Subtotal = "+this.subtotal.toString()+"$"
                +"\n Iva = "+this.iva.toString();
        
        if (this.totalDiscount > 0.0){
            ticket+="\n Discount = " + totalDiscount+"$" + "\n    ";
            ticket = this.discounts.stream().map((o) -> o.toString() + "\n    ").reduce(ticket, String::concat);
        }

        
        ticket +="\n Total = "+ this.total +"$";
        return ticket;
    }
}
